module Main where

import Data.Char (isDigit, digitToInt)
import Data.List (isPrefixOf, findIndex)
import Data.Maybe (catMaybes)
import Text.Printf (printf)

main :: IO()
main = do
  content <- lines <$> readFile "day01-input.txt"
  let solution1 = part1 content
  let solution2 = part2 content
  printf $ "Part1:\t" ++ show solution1 ++ "\n"
  printf $ "Part2:\t" ++ show solution2 ++ "\n"


part1 :: [String] -> Int
part1 = sum . map wordToNum

part2 :: [String] -> Int
part2 = sum . map (getFinalNumber . parseAllDigits)

wordToNum :: String -> Int
wordToNum xs =
  (digitToInt . head $ filter isDigit xs) * 10 +
  (digitToInt . last $ filter isDigit xs)


parseAllDigits :: [Char] -> [Maybe Int]
parseAllDigits [] = []
parseAllDigits s@(x:xs)
    | isDigit x = (Just . digitToInt) x : parseAllDigits xs
    | otherwise = ( (+1) <$> findIndex (`isPrefixOf` s) digits) : parseAllDigits xs
    where
        digits = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]


getFinalNumber :: [Maybe Int] -> Int
getFinalNumber xs = ((* 10) . head . catMaybes) xs + (last . catMaybes $ xs)
