module Main where

import Data.Map (Map)
import qualified Data.Map as Map
import Data.List (transpose)
import Data.Maybe (isNothing, fromMaybe, isJust, fromJust)



main :: IO()
main = do
  input <- readFile "./day10-input.txt"
  print $ part1 input



-- Define Grid instance and some of its utilities
data Grid = Grid { rows :: ![[Char]], my :: !Int, mx :: !Int }

instance Show Grid where
  show :: Grid -> String
  show (Grid rows _ _) = unlines rows

makeGrid :: String -> Grid
makeGrid s = Grid { rows = l, my = length l - 1, mx = length (head l) - 1 }
    where l = lines s

isOutOfBound :: Grid -> Int -> Int -> Bool
isOutOfBound grid y x = my grid < y || mx grid < x || x < 0 || y < 0

atPosition :: Grid -> (Int, Int) -> Maybe Char
atPosition grid (y, x)
  | isOutOfBound grid y x = Nothing
  | otherwise = Just $ rows grid !! y !! x

findEntryPoint :: Grid -> (Int, Int)
findEntryPoint grid = (y, x)
  where
    truthMap = map (== 'S') . concat $ rows grid
    idx = fst . head . filter snd $ zip [0..] truthMap
    y = idx `div` ( mx grid + 1 )
    x = idx `mod` ( mx grid + 1 )


-- Make pipes have a direction
data Direction = North | East | South | West
  deriving (Eq, Show)

inverse :: Direction -> Direction
inverse North = South
inverse South = North
inverse East = West
inverse West = East

-- Given a direction and a coordinate, gives the coordinate following the direction
nextCoordinate :: Direction -> (Int, Int) -> (Int, Int)
nextCoordinate North (y,x) = (y-1, x)
nextCoordinate South (y,x) = (y+1, x)
nextCoordinate East (y,x) = (y, x+1)
nextCoordinate West (y,x) = (y, x-1)

data Cell = Pipe {connections :: ![Direction]} | Ground | EntryPoint
  deriving (Eq, Show)

-- If the Cell is compatible with the direction in which we enter it, returns True
isCompatible :: Direction -> Cell -> Bool
isCompatible _ EntryPoint = True
isCompatible _ Ground = False
isCompatible d (Pipe ds) = inverse d `elem` ds

getNextDir :: Direction -> Cell -> Maybe Direction
getNextDir _ Ground = Nothing
getNextDir _ EntryPoint = Nothing
getNextDir d (Pipe [d1, d2])
  | trueDir == d1 = Just d2
  | otherwise = Just d1
  where trueDir = inverse d

-- takes id from starting point and try to make its way through the pipes
navigate :: Grid -> [Char] -> (Int, Int) -> Maybe Direction -> [Char]
navigate _ acc _ Nothing = acc
navigate grid acc (y, x) (Just d)
  | isCompatible d nextCell = navigate grid (nextChar : acc) nextCoor nextDir
  | otherwise = acc
    where
      nextCoor = nextCoordinate d (y,x)
      nextChar = fromMaybe '.' $ atPosition grid nextCoor
      nextCell = parseChar nextChar
      nextDir = getNextDir d nextCell



parseChar :: Char -> Cell
parseChar '.' = Ground
parseChar 'S' = EntryPoint
parseChar '|' = Pipe {connections = [North, South]}
parseChar '-' = Pipe {connections = [West, East]}
parseChar 'L' = Pipe {connections = [North, East]}
parseChar 'J' = Pipe {connections = [North, West]}
parseChar '7' = Pipe {connections = [South, West]}
parseChar 'F' = Pipe {connections = [South, East]}
parseChar c = error $ "Error while parsing map, unknown char: " ++ [c]


headMay :: [a] -> Maybe a
headMay (x:_) = Just x
headMay _ = Nothing

isJustSolved :: Maybe Char -> Bool
isJustSolved Nothing = False
isJustSolved (Just c) = c == 'S'

part1 :: String -> Int
part1 s = flip div 2 . length $ fromJust finalSol
  where
    maze = makeGrid s
    entryPoint = findEntryPoint maze
    dirs = map Just [North, East, South]
    solutions = map (navigate maze "" entryPoint) dirs
    validSolves = filter (isJustSolved . headMay) solutions
    finalSol = headMay validSolves
