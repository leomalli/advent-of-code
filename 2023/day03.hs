import Data.Char (isDigit)
import Data.Maybe (catMaybes)
import Data.List (concatMap, groupBy, sortBy, intersect, null, nub)
import Data.Function (on)
import Text.Printf (printf)


main :: IO ()
main = do
    content <- readFile "day03-input.txt"
    let sol1 = part1 content
    let sol2 = part2 content
    printf $ "Part1:\t" ++ show sol1 ++ "\n"
    printf $ "Part2:\t" ++ show sol2 ++ "\n"


part1 :: String -> Int
part1 ss = sum $ getParts grid coordinates []
  where
    grid = sanitiseGrid $ makeGrid ss
    coordinates = [ (y, x) | y <- [0.. my grid], x <- [0.. mx grid] ]

part2 :: String -> Int
part2 ss = sum $ map (computeRatio grid) actualGears
  where
    grid = makeGrid ss
    potentialGears = findPotentialGears grid
    actualGears = filter (isGear grid) $ zip potentialGears $ map (neighboringDigits grid) potentialGears

data Grid = Grid { rows :: ![String], my :: !Int, mx :: !Int }
instance Show Grid where show (Grid rows my mx) = show rows


makeGrid :: String -> Grid
makeGrid s = Grid { rows = l, my = length l - 1, mx = length (head l) - 1 }
    where l = lines s

-- Had problem where numbers at end of the line would leak into next line, just adding empty col at the end should do the trick...
sanitiseGrid :: Grid -> Grid
sanitiseGrid g = Grid { rows = map (++".") (rows g), my = my g, mx = mx g + 1 }


data Cell = Cell { cc :: !Char, cy :: !Int, cx :: !Int } deriving (Eq)
instance Show Cell where show (Cell cc cy cx) = cc : ":(" ++ show cy ++ ", " ++ show cx ++ ")"


isOutOfBound :: Grid -> Int -> Int -> Bool
isOutOfBound grid y x = my grid < y || mx grid < x || x < 0 || y < 0


getCell :: Grid -> Int -> Int -> Maybe Cell
getCell grid y x
    | isOutOfBound grid y x = Nothing
    | otherwise = Just Cell { cc = rows grid !! y !! x, cy = y, cx = x }


unsafeGetCell :: Grid -> Int -> Int -> Cell
unsafeGetCell grid y x = Cell { cc = rows grid !! y !! x, cy = y, cx = x }


neighbors :: Grid -> Int -> Int -> [Cell]
neighbors grid y x = catMaybes $ [ getCell grid (y+dy) (x+dx) | dx <- [-1, 0, 1], dy <- [-1, 0, 1] ]


hasSymbolNeighbor :: Grid -> Int -> Int -> Bool
hasSymbolNeighbor g y x = any (isSymbol . cc) $ neighbors g y x


isSymbol :: Char -> Bool
isSymbol c = not $ isDigit c || c == '.' -- || c == '\n'


-- Accumulate the numbers and keep track of if they have a symbol or not as neighbor
getParts :: Grid -> [(Int, Int)] -> [(Bool, String)] -> [Int]
getParts g c [] = getParts g c [(False, "")]
getParts _ [] acc = map (read . reverse . snd) $ filter fst acc
getParts g ((y,x):coords) acc
    | (isDigit . cc ) $ unsafeGetCell g y x = getParts g coords ((symbolStatus, cc (unsafeGetCell g y x) : s):cs)
    | otherwise = getParts g coords ((False, ""):acc)
    where
        ((b, s):cs) = acc
        symbolStatus = b || hasSymbolNeighbor g y x


findPotentialGears :: Grid -> [Cell]
findPotentialGears g = [ c | y <- [0.. my g], x <- [0.. mx g], let c = unsafeGetCell g y x, cc c == '*']


isGear :: Grid -> (Cell, [Cell]) -> Bool
isGear g (sym,ns)
    | length ns < 2 = False
    | length asLines > 2 = False
    | otherwise = sum (map lineToNumberCount asLines) == 2
    where
        asLines = groupByLines ns

neighboringDigits :: Grid -> Cell -> [Cell]
neighboringDigits grid c = filter (isDigit . cc) $ catMaybes $ [ getCell grid (y+dy) (x+dx) | dx <- [-1, 0, 1], dy <- [-1, 0, 1] ]
    where
        y = cy c
        x = cx c

-- Takes a Gear and compute its ratio
computeRatio :: Grid -> (Cell, [Cell]) -> Int
computeRatio g (gear, xs) = product $ map cellListToNum $ concatMap (extractNumbersFromLine g xs) lines
    where
        lines = nub [ cy c | c <- xs ]

cellListToNum :: [Cell] -> Int
cellListToNum cs = read $ map cc cs

extractNumbersFromLine :: Grid -> [Cell] -> Int -> [[Cell]]
extractNumbersFromLine g nds y = filter (not . null . intersect nds) $ filter (isDigit . cc . head) $ groupBy (\c1 c2 -> isDigit (cc c1) && isDigit (cc c2)) $ map (unsafeGetCell g y) [0 .. mx g]

lineToNumberCount :: [Cell] -> Int
lineToNumberCount cs
    | length cs == 3 = 1
    | length cs == 1 = 1
    | otherwise = abs ( (cx . head) cs - (cx .last) cs)

-- We first sort them by line to ensure corect results
groupByLines :: [Cell] -> [[Cell]]
groupByLines = groupBy ((==) `on` cy) . sortBy (compare `on` cy)
