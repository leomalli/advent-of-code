module Main where

main :: IO()
main = do
    content <- readFile "day06-input.txt"
    let sol1 = part1 content
    let sol2 = part2 content
    print [sol1, sol2]

possibilites :: Int -> [Int]
possibilites x = [ d | v <- [0..x], let d = (x-v)*v ]

part2 :: [Char] -> Int
part2 ct = length $ filter (>record) $ possibilites time
    where
        [time, record] = map (read @Int . concat . tail . words) $ lines ct


part1 :: [Char] -> Int
part1 ct = product $ zipWith (\r ts -> length $ filter (>r) ts) records $ map (\ x -> [ d | v <- [0..x], let d = (x-v)*v ]) times
    where
        [times, records] = map (map (read @Int) . tail . words) $ lines ct
