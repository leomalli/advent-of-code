-- Hey, I got the first part of the day working just fine but couldn't manage to optimise it enough for it to work on part2 of the day...
-- So I took it as an oportunity to learn things outside of my comfort zone and looked at someone else's code:
--      https://github.com/gruhn/advent-of-code/blob/master/2023/Day05.hs


module Main where
import Data.Function (on)
import Data.Semigroup (sconcat)
import Data.List (groupBy, sortOn)
import Data.List.NonEmpty (NonEmpty)
import qualified Data.List.NonEmpty as NonEmpty
import Control.Monad (guard)
import Data.Maybe (listToMaybe, mapMaybe)

data Range = Range
    {
        getDst :: !Int,
        getSrc :: !Int,
        getLen :: !Int
    } deriving Show

newtype RangeMap = RangeMap { getRanges :: [Range] }
    deriving Show


-- Parse logic
parse :: [String] -> ([Int], [RangeMap])
parse (s:_:ss) = (seeds, RangeMap <$> parseMaps ss)
    where
        seeds = map (read @Int) . tail . words $ s

parseMaps :: [String] -> [[Range]]
parseMaps = map (map (toRange . map (read @Int) . words) . tail) . filter (/=[""]) . groupBy ((&&) `on` (/=""))
    where
        toRange :: [Int] -> Range
        toRange [a, b, c] = Range a b c


-- Helper
nonEmpty :: Range -> Bool
nonEmpty range = getLen range > 0


-- Make RangeMap into a semigroup in order to concat them easier
fillRanges :: RangeMap -> RangeMap
fillRanges (RangeMap ranges) =
    let fill :: Int -> [Range] -> [Range]
        -- Acts as the last range (maxVal, +inf) in a sense
        fill idx []             = [Range idx idx (maxBound - idx)]
        fill idx (range:ranges) =
            let fillBefore = Range idx idx (getSrc range - idx)
                newIndex   = getSrc range + getLen range
            in fillBefore : range : fill newIndex ranges
    in RangeMap $ filter nonEmpty $ fill 0 $ sortOn getSrc ranges

-- Idea is to find what ranges intersect and compute the resulting ranges
instance Semigroup RangeMap where
    (RangeMap rangesA) <> (RangeMap rangesB) = RangeMap $ do
        -- Parse range
        Range dstA srcA lenA <- rangesA
        Range dstB srcB lenB <- rangesB
        -- Find intersection:
        --          [ srcA : dstA ] -> [ srcB -> dstB ] ==> [ srcA -> dstB ]
        let diffLeft = dstA - srcB
            -- lenInter = max 0 $ min (srcA + lenA) (srcB + lenB) - srcB - max 0 diffLeft
            dst = dstB + max 0 (dstA - srcB) -- (dstA, dstA + lenA) \cap (srcB, srcB + lenB)
            src = srcA + max 0 (srcB - dstA)
            len = max 0 $ min (srcA + lenA - src) (dstB + lenB - dst)

            final    = Range dst src len

        guard $ nonEmpty final
        return final


-- Create RangeMap from the Seeds
rangeMapFromSeeds :: [Int] -> RangeMap
rangeMapFromSeeds = RangeMap . transform
    where
        -- Small helper
        transform :: [Int] -> [Range]
        transform []              = []
        transform (src:len:seeds) = Range src src len : transform seeds

apply :: RangeMap -> Int -> Maybe Int
apply (RangeMap ranges) x = listToMaybe $ do
    Range dst src len <- ranges
    guard $ (x >= src) && (x < src + len)
    return (dst + x - src)



main :: IO()
main = do
    content <- readFile "day05-input.txt"
    print $ part1 content
    print $ part2 content
    return ()

part1 :: [Char] -> Int
part1 cs = minimum $ mapMaybe (apply finalRange) seeds
    where
        (seeds, mapsFuncs) = parse $ lines cs
        finalRange = sconcat $ NonEmpty.fromList $ map fillRanges mapsFuncs

part2 :: [Char] -> Int
part2 cs = minimum $ map getDst $ getRanges $ rangeMapFromSeeds seeds <> finalRange
    where
        (seeds, mapsFuncs) = parse $ lines cs
        finalRange = sconcat $ NonEmpty.fromList $ map fillRanges mapsFuncs
