import Data.List (groupBy, intersect)
import Data.Char (isDigit)
import Data.Function (on)
import Text.Printf (printf)

main :: IO()
main = do
    content <- lines <$> readFile "day04-input.txt"
    let sol1 = part1 content
    let sol2 = part2 content
    printf $ "Part1:\t" ++ show sol1 ++ "\n"
    printf $ "Part2:\t" ++ show sol2 ++ "\n"


part1 :: [String] -> Int
part1 = sum . map (gameScore . readGame . parseGame)

part2 :: [String] -> Int
part2 = sum . foldGame . map (gameMatches . readGame . parseGame)

parseGame :: [Char] -> [[Char]]
parseGame = tail . foldr (\c (s:ss) -> if c `elem` ['|', ':'] then "":(s:ss) else (c:s):ss) [""]


readGame :: [String] -> [[String]]
readGame = map (filter (isDigit . head) . groupBy ((==) `on` isDigit))


gameScore :: [[String]] -> Int
gameScore [cards, nums] = if matches > 0 then 2^(matches - 1) else 0
    where
        matches = length $ intersect cards nums


gameMatches :: [[String]] -> Int
gameMatches [cards, nums] = length $ intersect cards nums


foldGame :: [Int] -> [Int]
foldGame xs = foldGameIntern [1 | _ <- xs] xs

foldGameIntern :: [Int] -> [Int] -> [Int]
foldGameIntern cs [] = cs
foldGameIntern (c:cs) (m:ms) = c : foldGameIntern (zipWith (+) (constructZip m c) cs) ms

-- Need to pad the end of the zipped list with the neutral element of (+) in order not to cut short the cards list
constructZip :: Int -> Int -> [Int]
constructZip m c = take m (num c) ++ num 0


-- Infinite list of x's, [x, x, x, ...]
num :: Int -> [Int]
num x = x : num x
