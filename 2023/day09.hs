module Main where


adjacentDiff :: [Int] -> [Int]
adjacentDiff xs = zipWith (-) (tail xs) xs

parse :: [String] -> [[Int]]
parse = map (map (read @Int) . words)

nextValue :: [Int] -> Int
nextValue xs
    | all (==0) subLevel = last xs
    | otherwise = last xs + nextValue subLevel
    where
        subLevel = adjacentDiff xs

part1 :: [Char] -> Int
part1 = sum . map nextValue . parse . lines

part2 :: [Char] -> Int
part2 = sum . map (nextValue . reverse) . parse . lines

main :: IO()
main = do
    content <- readFile "day09-input.txt"
    print $ part1 content
    print $ part2 content



sample :: [String]
sample = [
    "0 3 6 9 12 15",
    "1 3 6 10 15 21",
    "10 13 16 21 30 45"
    ]
