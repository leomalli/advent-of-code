module Main where

import Text.Printf (printf)

main :: IO ()
main = do
  content <- lines <$> readFile "day12-input.txt"
  let sol1 = part1 content
  let sol2 = part2 content
  printf $ "Part1:\t" ++ show sol1 ++ "\n"
  printf "Part2 will hang nearly forever...\n"
  printf $ "Part2:\t" ++ show sol2 ++ "\n"

{- PART 2 -}

part2 :: [String] -> Int
part2 = sum . map (possibleChoices . unfoldRecord)

unfoldRecord :: String -> String
unfoldRecord ss = concat $ replicate 5 (ms ++ "?") ++ [" "] ++ replicate 5 (xs ++ ",")
  where
    [ms, xs] = splitOn " " ss





{-  PART 1 -}

part1 :: [String] -> Int
part1 = sum . map possibleChoices

type SpringInfos = [Int]

data SpringCondition
  = Operational
  | Damaged
  | Unknown
  deriving (Eq)

instance Show SpringCondition where
  show :: SpringCondition -> String
  show Operational = "."
  show Damaged = "#"
  show Unknown = "?"

charToSpringCondition :: Char -> SpringCondition
charToSpringCondition '.' = Operational
charToSpringCondition '#' = Damaged
charToSpringCondition '?' = Unknown
charToSpringCondition c =
  error $ "Error while parsing spring condition, unknown type:\t" ++ [c, '\n']

type SpringMap = [SpringCondition]

parseToSprings :: String -> (SpringMap, SpringInfos)
parseToSprings ss =
  (map charToSpringCondition xs, map (read @Int) $ words cleanedYs)
  where
    (xs, ys) = break (== ' ') ss
    cleanedYs = map (\x -> if x == ',' then ' ' else x) ys

isValidMap :: SpringMap -> SpringInfos -> Bool
isValidMap sMap sInfos
  | Unknown `elem` sMap = error "There is uncertainty, cannot validate map\n"
  | otherwise = sInfos == map length splited
  where
    splited = filter (not . null) $ splitOn [Operational] sMap

splitOn :: (Eq a) => [a] -> [a] -> [[a]]
splitOn _ [] = [[]]
splitOn splits xs =
  a :
  if null b
    then []
    else splitOn splits (dropWhile (`elem` splits) b)
  where
    (a, b) = break (`elem` splits) xs

choices :: SpringMap -> [[SpringCondition]]
choices = map choice
  where
    choice :: SpringCondition -> [SpringCondition]
    choice x = if x == Unknown then [Damaged, Operational] else [x]


possibleChoices :: String -> Int
possibleChoices ss = length $ filter (`isValidMap` sInfos) allChoices
  where
    (sMap, sInfos) = parseToSprings ss
    allChoices = sequence $ choices sMap

sample :: [String]
sample =
  [ "???.### 1,1,3"
  , ".??..??...?##. 1,1,3"
  , "?#?#?#?#?#?#?#? 1,3,1,6"
  , "????.#...#... 4,1,1"
  , "????.######..#####. 1,6,5"
  , "?###???????? 3,2,1"
  ]
