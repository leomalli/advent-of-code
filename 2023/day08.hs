module Main where

import qualified Data.Map as M
import Control.Applicative (liftA2)
import Data.Char (isAlphaNum, isSpace)
import Data.Maybe (fromJust)
import Data.List (nub)

type Key = String
type Node = (Key, Key)
type Network = M.Map String Node


parsing :: [String] -> (String, Network)
parsing xs = (instructions, network)
    where
        instructions = cycle . head $ xs
        network = M.fromList $ map parseEntry $ drop 2 xs

parseEntry :: String -> (Key, Node)
parseEntry str = toCorrectFormat $ words $ filter (liftA2 (||) isAlphaNum isSpace) str
    where
        toCorrectFormat [a, b, c] = (a, (b, c))

navigate' :: (String, Network) -> Key -> Int -> Int
navigate' _ "ZZZ" x = x
navigate' (s:ss, net) key x
    | s == 'L'  = navigate' (ss, net) (fst res) (x+1)
    | otherwise = navigate' (ss, net) (snd res) (x+1)
    where
        res = fromJust $ M.lookup key net

navigate :: (String, Network)  -> Int
navigate net = navigate' net "AAA" 0

part1 :: [Char] -> Int
part1 cs = navigate (parsing input)
    where
        input = lines cs


-- This feels a bit cheap to suppose that there will be cycles...
-- But I left the full solution below, after trying to run it and optimise it I caved
-- and simplified everything to a lcm with the above assumption...
part2 :: [Char] -> Int
-- part2 cs = navigateFrom (instructions, network) startKeys 0
part2 cs = foldr (lcm . navigateFrom (instructions, network) 0) 1 startKeys
    where
        input = lines cs
        (instructions, network) = parsing input
        startKeys = filter ((=='A') . last) $ M.keys network

navigateFrom :: (String, Network) -> Int -> Key -> Int
navigateFrom (s:ss, net) x key
    | last key == 'Z' = x
    | s == 'L'        = navigateFrom (ss, net) (x+1) (fst updatedKey)
    | otherwise       = navigateFrom (ss, net) (x+1) (snd updatedKey)
    where
        updatedKey = net M.! key

-- navigateFrom :: (String, Network) -> [Key] -> Int -> Int
-- navigateFrom (s:ss, net) keys x
--     | all ((=='Z') . last) keys = x
--     | s == 'L'  = navigateFrom (ss, net) (nub $ map fst updatedKeys) (x+1)
--     | otherwise = navigateFrom (ss, net) (nub $ map snd updatedKeys) (x+1)
--     where
--         updatedKeys = map (fromJust . flip M.lookup net) keys


main :: IO()
main = do
    content <- readFile "day08-input.txt"
    let res1 = part1 content
    print $ part1 content
    let res2 = part2 content
    print $ part2 content
