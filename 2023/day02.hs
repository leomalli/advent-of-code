import Data.Char (isDigit, digitToInt)
import Text.Printf (printf)

data Rules = Rules {
    red   :: !Int,
    green :: !Int,
    blue  :: !Int
    } deriving (Eq, Show, Read)

rule :: Rules
rule = Rules {red = 12, green = 13, blue = 14}


main :: IO()
main = do
    content <- lines <$> readFile "day02-input.txt"
    let sol1 = part1 content
    let sol2 = part2 content
    printf $ "Part1:\t" ++ show sol1 ++ "\n"
    printf $ "Part2:\t" ++ show sol2 ++ "\n"

part1 :: [String] -> Int
part1 = sum . map fst . filter (testGame rule . snd) . zip [1..]

part2 :: [String] -> Int
part2 = sum . map computePower


testGame :: Rules -> [Char] -> Bool
testGame rule game = testRules rule (accumulateGame [0,0,0] (parseGame game))
    where
        testRules :: Rules -> [Int] -> Bool
        testRules x (r:g:b:_) = red x >= r && green x>= g && blue x >= b

accumulateGame :: [Int] -> [[Char]] -> [Int]
accumulateGame cs [] = cs
accumulateGame (r:g:b:_) (x1:xs)
    | (isDigit . head) x1 && head xs == "red" = accumulateGame [max r (read x1),g,b] xs
    | (isDigit . head) x1 && head xs == "green" = accumulateGame [r,max g (read x1),b] xs
    | (isDigit . head) x1 && head xs == "blue" = accumulateGame [r,g,max b (read x1)] xs
    | otherwise = accumulateGame [r, g, b] xs


parseGame :: [Char] -> [[Char]]
parseGame = lines . cleanStr

cleanStr :: [Char] -> [Char]
cleanStr [] = []
cleanStr (x:xs)
    | x `elem` [',', ':', ';'] = cleanStr xs
    | x == ' ' = '\n' : cleanStr xs
    | otherwise = x : cleanStr xs


computePower :: [Char] -> Int
computePower game = product $ accumulateGame [0,0,0] (parseGame game)
