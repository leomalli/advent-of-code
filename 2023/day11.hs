module Main where

import Data.List (tails, transpose)
import Text.Printf (printf)

type Coordinate = (Int, Int)

main :: IO ()
main = do
  content <- lines <$> readFile "day11-input.txt"
  let sol1 = part1 content
  let sol2 = part2 content
  printf $ "Part1:\t" ++ show sol1 ++ "\n"
  printf $ "Part2:\t" ++ show sol2 ++ "\n"

{- PART 2 STUFF -}

part2 :: [String] -> Int
part2 ss = sum $ map (computeExpandedDistance expMap 1000000) pairs
  where
    expMap = partsToExpand ss
    galaxies = findGalaxies ss
    pairs = [(i, j) | (i:rest) <- tails galaxies, j <- rest]


-- loop the galaxy and find where we need expansions
partsToExpandHor :: [String] -> [Int]
partsToExpandHor = map fst . filter (not . ('#' `elem`) . snd) . zip [0..]

partsToExpand :: [String] -> ([Int], [Int])
partsToExpand ss = (ys, xs)
  where
    ys = partsToExpandHor ss
    xs = partsToExpandHor $ transpose ss

data Range = Range {lower :: !Int, upper :: !Int}

makeRange :: Int -> Int -> Range
makeRange a b
  | a < b = Range {lower = a, upper = b}
  | otherwise = Range {lower = b, upper = a}

isInRange :: Int -> Range -> Bool
isInRange x (Range a b) = (x >= a) && (x <= b)


computeExpandedDistance :: ([Int], [Int]) -> Int -> (Coordinate, Coordinate) -> Int
computeExpandedDistance (ys, xs) expFactor ((y1, x1), (y2, x2)) = initialDistance + (expFactor - 1) * xOverlap + (expFactor - 1) * yOverlap
  where
    initialDistance = abs (x1 - x2) + abs (y1 - y2)
    rangeX = makeRange x1 x2
    rangeY = makeRange y1 y2
    xOverlap = length $ filter (`isInRange` rangeX) xs
    yOverlap = length $ filter (`isInRange` rangeY) ys


{- PART 1 STUFF -}

part1 :: [String] -> Int
part1 ss = sum $ map computeDistance pairs
  where
    expUniv = expandUniverseBy 2 ss
    galaxies = findGalaxies expUniv
    pairs = [(i, j) | (i:rest) <- tails galaxies, j <- rest]

expandUniverseBy :: Int -> [String] -> [String]
expandUniverseBy n =
  transpose . expandHorizontalBy n . transpose . expandHorizontalBy n

expandHorizontalBy :: Int -> [String] -> [String]
expandHorizontalBy _ [] = []
expandHorizontalBy n (x:xs)
  | '#' `elem` x = x : expandHorizontalBy n xs
  | otherwise = replicate n x ++ expandHorizontalBy n xs

findGalaxies :: [String] -> [Coordinate]
findGalaxies grid = zip y x
  where
    truthMap = map (== '#') . concat $ grid
    idx = map fst . filter snd $ zip [0 ..] truthMap
    mx = length $ head grid
    y = map (`div` mx) idx
    x = map (`mod` mx) idx

computeDistance :: (Coordinate, Coordinate) -> Int
computeDistance ((y1, x1), (y2, x2)) = abs (x1 - x2) + abs (y1 - y2)
