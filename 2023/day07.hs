module Main where

import Data.Function (on)
import Data.List (sort, sortBy, group, groupBy)

data CardValue = Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten
                | Jack | Queen | King | Ace
                deriving (Show, Enum, Eq, Ord)

parseCardValue :: Char -> CardValue
parseCardValue '2' = Two
parseCardValue '3' = Three
parseCardValue '4' = Four
parseCardValue '5' = Five
parseCardValue '6' = Six
parseCardValue '7' = Seven
parseCardValue '8' = Eight
parseCardValue '9' = Nine
parseCardValue 'T' = Ten
parseCardValue 'J' = Jack
parseCardValue 'Q' = Queen
parseCardValue 'K' = King
parseCardValue 'A' = Ace

data Card = Card { value :: CardValue }
    deriving (Eq, Ord, Show)

parseCard :: Char -> Card
parseCard = Card . parseCardValue

parseHand :: String -> [Card]
parseHand = map parseCard

data PokerHand = HighCard | OnePair | TwoPair | ThreeOfAKind |
                 FullHouse | FourOfAKind | FiveOfAKind
            deriving (Show, Eq, Ord)

countValues :: [Card] -> [(CardValue, Int)]
countValues = map (\xs -> (head xs, length xs)) . group . sort . map value

pokerHand :: [Card] -> PokerHand
pokerHand hand
    | any ((==5) . snd) valueCount = FiveOfAKind
    | any ((==4) . snd) valueCount = FourOfAKind
    | any ((==3) . snd) valueCount && any ((==2) . snd) valueCount = FullHouse
    | any ((==3) . snd) valueCount = ThreeOfAKind
    | length pairs == 2 = TwoPair
    | length pairs == 1 = OnePair
    | otherwise = HighCard
    where
        valueCount = countValues hand
        pairs = filter ((==2) . snd) valueCount


compareHands :: String -> String -> Ordering
compareHands = compare `on` (pokerHand . parseHand)

lexicoCompareHands :: String -> String -> Ordering
lexicoCompareHands = compare `on` parseHand

sortBids :: [(String, Int)] -> [(String, Int)]
sortBids = concatMap (sortBy (lexicoCompareHands `on` fst)) . groupBy ((==) `on` pokerHand . parseHand . fst) . sortBy (compareHands `on` fst)

part1 :: [Char] -> Int
part1 cs =  sum $ zipWith (*) [1..] (map snd sorted)
    where
        sorted = (sortBids . map ((\[a,b] -> (a,read  @Int b)) . words) . lines) cs

main :: IO()
main = do
    content <- readFile "day07-input.txt"
    print $ part1 content
