module Main where

import qualified Data.Bifunctor
import Data.Maybe (fromMaybe)
import Data.List (transpose)
import Text.Printf (printf)

main :: IO()
main = do
  content <- lines <$> readFile "day13-input.txt"
  let sol1 = part1 content
  printf $ "Part1:\t" ++ show sol1 ++ "\n"


part1 :: [String] -> Int
part1 = foldl (\b a -> b + findReflection a) 0 . parsePattern


findReflection :: [String] -> Int
findReflection ss = findHorizontalReflection ss + findHorizontalReflection (transpose ss) * 100

-- Returns zero if nothing found
findHorizontalReflection :: [String] -> Int
findHorizontalReflection xs = fromMaybe 0 . headMay . foldl possibleReflection [1..l] $ xs
  where
    l = length (head xs) - 1

possibleReflection :: [Int] -> String -> [Int]
possibleReflection [] _ = []
possibleReflection choices ss = map fst $ filter (all (uncurry (==)) . snd) zipedSplits
  where
    possibleSplits = map (flip splitAt ss) choices
    reversedSplits = map (Data.Bifunctor.first reverse) possibleSplits
    zipedSplits = zip choices $ map (uncurry zip) reversedSplits


parsePattern :: [String] -> [[String]]
parsePattern = splitOn [""]

splitOn :: (Eq a) => [a] -> [a] -> [[a]]
splitOn _ [] = [[]]
splitOn splits xs =
  a :
  if null b
    then []
    else splitOn splits (dropWhile (`elem` splits) b)
  where
    (a, b) = break (`elem` splits) xs

headMay :: [a] -> Maybe a
headMay [] = Nothing
headMay (x:_) = Just x
